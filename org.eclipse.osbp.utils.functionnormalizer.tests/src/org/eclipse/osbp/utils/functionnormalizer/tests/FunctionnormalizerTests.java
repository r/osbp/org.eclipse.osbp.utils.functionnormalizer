/*
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */

package org.eclipse.osbp.utils.functionnormalizer.tests;

import static org.junit.Assert.assertEquals;

import org.eclipse.osbp.utils.functionnormalizer.api.FunctionTypingAPI;
import org.junit.Test;

// TODO: Auto-generated Javadoc
/**
 * Tests the {@link TextFieldPresentation}.
 */
public class FunctionnormalizerTests {


	/**
	 * Tests rendering issues.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_FunctionTypingAPI() throws Exception {
		
		FunctionTypingAPI functionTypingAPI = new FunctionTypingAPI();

		assertEquals("functionCss", functionTypingAPI.getFunctionTypeNameByListIndex(0));
		assertEquals("functionConverter", functionTypingAPI.getFunctionTypeNameByListIndex(1));
		assertEquals("functionImagePicker", functionTypingAPI.getFunctionTypeNameByListIndex(2));

		assertEquals("functionCss", functionTypingAPI.getFunctionCssTypeName());
		assertEquals("functionConverter", functionTypingAPI.getFunctionConverterTypeName());
		assertEquals("functionImagePicker", functionTypingAPI.getFunctionImagePickerTypeName());
		
		assertEquals("booleanFunction", functionTypingAPI.getFunctionNormalizerNameByListIndex(0));
		assertEquals("StringFunction", functionTypingAPI.getFunctionNormalizerNameByListIndex(1));
		
		assertEquals("boolean", functionTypingAPI.getFunctionNormalizerReturnTypeByListIndex(0));
		assertEquals("String", functionTypingAPI.getFunctionNormalizerReturnTypeByListIndex(1));
		
		assertEquals("boolean", functionTypingAPI.getFunctionNormalizerReturnTypeByName("booleanFunction"));
		assertEquals("String", functionTypingAPI.getFunctionNormalizerReturnTypeByName("StringFunction"));
	}

}
