package org.eclipse.osbp.utils.functionnormalizer.dtos;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.Locale;
import org.eclipse.osbp.runtime.common.i18n.ITranslator;
import org.eclipse.osbp.utils.functionnormalizer.entities.FunctionnormalizerEntitiesTranslator;
import org.slf4j.Logger;

@SuppressWarnings("all")
public class FunctionnormalizerDtosTranslator implements ITranslator {
  private static Logger log = org.slf4j.LoggerFactory.getLogger("translations by translator");
  
  private static FunctionnormalizerDtosTranslator instance = null;
  
  private static Locale lastLocale = null;
  
  private static FunctionnormalizerEntitiesTranslator functionnormalizerEntitiesTranslator = null;
  
  private PropertyChangeSupport pcs = new java.beans.PropertyChangeSupport(this);
  
  private HashMap<String, String> translations = new HashMap<String,String>() {{
    }};
  
  public static FunctionnormalizerDtosTranslator getInstance(final Locale locale) {
    if(instance == null) {
    	instance = new org.eclipse.osbp.utils.functionnormalizer.dtos.FunctionnormalizerDtosTranslator();
    	if (lastLocale == null) {
    		instance.changeLocale(locale);
    	}
    }
    return instance;
  }
  
  public void addPropertyChangeListener(final PropertyChangeListener listener) {
    this.pcs.addPropertyChangeListener(listener);
  }
  
  public void removePropertyChangeListener(final PropertyChangeListener listener) {
    this.pcs.removePropertyChangeListener(listener);
  }
  
  public void mergeTranslations() {
    functionnormalizerEntitiesTranslator.mergeTranslations();
    translations.putAll(functionnormalizerEntitiesTranslator.getTranslations());
    if (log.isDebugEnabled()) log.debug("merge FunctionnormalizerEntitiesTranslator in org.eclipse.osbp.utils.functionnormalizer.dtos");
    
  }
  
  public HashMap<String, String> getTranslations() {
    return translations;
  }
  
  public void changeLocale(final Locale locale) {
    // avoid unnecessary settings
    if (locale == null) {
    	if (log.isDebugEnabled()) log.debug("locale for org.eclipse.osbp.utils.functionnormalizer.dtos is null.");
    	return;
    }
    if (locale.equals(lastLocale)) {
    	if (log.isDebugEnabled()) log.debug("locale for org.eclipse.osbp.utils.functionnormalizer.dtos already set to "+locale.getDisplayLanguage());
    	return;
    }
    if (log.isDebugEnabled()) log.debug("locale for org.eclipse.osbp.utils.functionnormalizer.dtos set to "+locale.getDisplayLanguage());
    lastLocale = locale;
    // call the imported translators change locale method
    if (functionnormalizerEntitiesTranslator == null) {
    	functionnormalizerEntitiesTranslator = functionnormalizerEntitiesTranslator.getInstance(locale);
    } else {
    	functionnormalizerEntitiesTranslator.changeLocale(locale);
    }
    try {
    	java.util.ResourceBundle resource = java.util.ResourceBundle.getBundle("i18n.I18N", locale, getClass().getClassLoader());
    } catch (java.util.MissingResourceException mre) {
    	System.err.println(getClass().getCanonicalName()+" - "+mre.getLocalizedMessage());
    }
    
  }
}
