/**
 * Copyright (c) 2015 - 2016 - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Loetz GmbH&Co.KG - Initial implementation
 */
package org.eclipse.osbp.utils.functionnormalizer.dtos.mapper;

import java.util.List;
import org.eclipse.osbp.dsl.dto.lib.IMapper;
import org.eclipse.osbp.dsl.dto.lib.IMapperAccess;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.eclipse.osbp.utils.functionnormalizer.dtos.FunctionNormalizerDto;
import org.eclipse.osbp.utils.functionnormalizer.dtos.FunctionTypeDto;
import org.eclipse.osbp.utils.functionnormalizer.dtos.FunctionTypingDto;
import org.eclipse.osbp.utils.functionnormalizer.entities.FunctionNormalizer;
import org.eclipse.osbp.utils.functionnormalizer.entities.FunctionType;
import org.eclipse.osbp.utils.functionnormalizer.entities.FunctionTyping;

/**
 * This class maps the dto {@link FunctionTypingDto} to and from the entity {@link FunctionTyping}.
 * 
 */
@SuppressWarnings("all")
public class FunctionTypingDtoMapper<DTO extends FunctionTypingDto, ENTITY extends FunctionTyping> implements IMapper<DTO, ENTITY> {
  private IMapperAccess mapperAccess;
  
  /**
   * Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.
   * 
   * @param dtoClass - the class of the dto that should be mapped
   * @param entityClass - the class of the entity that should be mapped
   * @return the mapper instance or <code>null</code>
   */
  protected <D, E> IMapper<D, E> getToDtoMapper(final Class<D> dtoClass, final Class<E> entityClass) {
    return mapperAccess.getToDtoMapper(dtoClass, entityClass);
  }
  
  /**
   * Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.
   * 
   * @param dtoClass - the class of the dto that should be mapped
   * @param entityClass - the class of the entity that should be mapped
   * @return the mapper instance or <code>null</code>
   */
  protected <D, E> IMapper<D, E> getToEntityMapper(final Class<D> dtoClass, final Class<E> entityClass) {
    return mapperAccess.getToEntityMapper(dtoClass, entityClass);
  }
  
  /**
   * Called by OSGi-DS. Binds the mapper access service.
   * 
   * @param service - The mapper access service
   * 
   */
  protected void bindMapperAccess(final IMapperAccess mapperAccess) {
    this.mapperAccess = mapperAccess;
  }
  
  /**
   * Called by OSGi-DS. Binds the mapper access service.
   * 
   * @param service - The mapper access service
   * 
   */
  protected void unbindMapperAccess(final IMapperAccess mapperAccess) {
    this.mapperAccess = null;
  }
  
  /**
   * Creates a new instance of the entity
   */
  public FunctionTyping createEntity() {
    return new FunctionTyping();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public FunctionTypingDto createDto() {
    return new FunctionTypingDto();
  }
  
  /**
   * Maps the entity {@link FunctionTyping} to the dto {@link FunctionTypingDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final FunctionTypingDto dto, final FunctionTyping entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
  }
  
  /**
   * Maps the dto {@link FunctionTypingDto} to the entity {@link FunctionTyping}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final FunctionTypingDto dto, final FunctionTyping entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    
    if(entity.getTypes().isEmpty()) {
    	List<FunctionType> types_entities = new java.util.ArrayList<FunctionType>();
    	for(FunctionType _entityValue : toEntity_types(dto, context)) {
    		types_entities.add(_entityValue);
    	}
    	entity.setTypes(types_entities);
    }
    if(entity.getNormalizer().isEmpty()) {
    	List<FunctionNormalizer> normalizer_entities = new java.util.ArrayList<FunctionNormalizer>();
    	for(FunctionNormalizer _entityValue : toEntity_normalizer(dto, context)) {
    		normalizer_entities.add(_entityValue);
    	}
    	entity.setNormalizer(normalizer_entities);
    }
  }
  
  /**
   * Maps the property types from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped dtos
   * 
   */
  protected List<FunctionTypeDto> toDto_types(final FunctionTyping in, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<FunctionTypeDto, FunctionType> mapper = getToDtoMapper(FunctionTypeDto.class, FunctionType.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    } 
    
    if (context.isDetectRecursion(FunctionTypeDto.class)) {
    	return java.util.Collections.emptyList();
    }
    
    context.increaseLevel();
    List<FunctionTypeDto> results = new java.util.ArrayList<FunctionTypeDto>();
    for (FunctionType _entity : in.getTypes()) {
    	FunctionTypeDto _dto = context.get(mapper.createDtoHash(_entity));
    	if (_dto == null) {
    		_dto = mapper.createDto();
    		mapper.mapToDTO(_dto, _entity, context);
    	} else {
    		if(context.isRefresh()){
    			mapper.mapToDTO(_dto, _entity, context);
    		}
    	}
    	results.add(_dto);
    }
    context.decreaseLevel();
    return results;
  }
  
  /**
   * Maps the property types from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param context - The context to get information about depth,...
   * @return A list of mapped entities
   * 
   */
  protected List<FunctionType> toEntity_types(final FunctionTypingDto in, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<FunctionTypeDto, FunctionType> mapper = getToEntityMapper(FunctionTypeDto.class, FunctionType.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    }
    
    List<FunctionType> results = new java.util.ArrayList<FunctionType>();
    for (FunctionTypeDto _dto : in.getTypes()) {
    	FunctionType _entity = mapper.createEntity();
    	mapper.mapToEntity(_dto, _entity, context);
    	results.add(_entity);
    }
    return results;
  }
  
  /**
   * Maps the property normalizer from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped dtos
   * 
   */
  protected List<FunctionNormalizerDto> toDto_normalizer(final FunctionTyping in, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<FunctionNormalizerDto, FunctionNormalizer> mapper = getToDtoMapper(FunctionNormalizerDto.class, FunctionNormalizer.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    } 
    
    if (context.isDetectRecursion(FunctionNormalizerDto.class)) {
    	return java.util.Collections.emptyList();
    }
    
    context.increaseLevel();
    List<FunctionNormalizerDto> results = new java.util.ArrayList<FunctionNormalizerDto>();
    for (FunctionNormalizer _entity : in.getNormalizer()) {
    	FunctionNormalizerDto _dto = context.get(mapper.createDtoHash(_entity));
    	if (_dto == null) {
    		_dto = mapper.createDto();
    		mapper.mapToDTO(_dto, _entity, context);
    	} else {
    		if(context.isRefresh()){
    			mapper.mapToDTO(_dto, _entity, context);
    		}
    	}
    	results.add(_dto);
    }
    context.decreaseLevel();
    return results;
  }
  
  /**
   * Maps the property normalizer from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param context - The context to get information about depth,...
   * @return A list of mapped entities
   * 
   */
  protected List<FunctionNormalizer> toEntity_normalizer(final FunctionTypingDto in, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<FunctionNormalizerDto, FunctionNormalizer> mapper = getToEntityMapper(FunctionNormalizerDto.class, FunctionNormalizer.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    }
    
    List<FunctionNormalizer> results = new java.util.ArrayList<FunctionNormalizer>();
    for (FunctionNormalizerDto _dto : in.getNormalizer()) {
    	FunctionNormalizer _entity = mapper.createEntity();
    	mapper.mapToEntity(_dto, _entity, context);
    	results.add(_entity);
    }
    return results;
  }
  
  public String createDtoHash(final Object in) {
    throw new UnsupportedOperationException("No id attribute available");
  }
  
  public String createEntityHash(final Object in) {
    throw new UnsupportedOperationException("No id attribute available");
  }
}
