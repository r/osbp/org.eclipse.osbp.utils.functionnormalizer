package org.eclipse.osbp.utils.functionnormalizer.entities;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.Locale;
import org.eclipse.osbp.runtime.common.i18n.ITranslator;
import org.slf4j.Logger;

@SuppressWarnings("all")
public class FunctionnormalizerEntitiesTranslator implements ITranslator {
  private static Logger log = org.slf4j.LoggerFactory.getLogger("translations by translator");
  
  private static FunctionnormalizerEntitiesTranslator instance = null;
  
  private static Locale lastLocale = null;
  
  private PropertyChangeSupport pcs = new java.beans.PropertyChangeSupport(this);
  
  private HashMap<String, String> translations = new HashMap<String,String>() {{
    		put("org.eclipse.osbp.utils.functionnormalizer.entities.function_normalizer","function_normalizer");
    		put("org.eclipse.osbp.utils.functionnormalizer.entities.function_type","function_type");
    		put("org.eclipse.osbp.utils.functionnormalizer.entities.function_typing","function_typing");
    		put("org.eclipse.osbp.utils.functionnormalizer.entities.name","name");
    		put("org.eclipse.osbp.utils.functionnormalizer.entities.normalizer","normalizer");
    		put("org.eclipse.osbp.utils.functionnormalizer.entities.return_type","return_type");
    		put("org.eclipse.osbp.utils.functionnormalizer.entities.types","types");
    }};
  
  private static String function_normalizer;
  
  private static String function_type;
  
  private static String function_typing;
  
  private static String name;
  
  private static String normalizer;
  
  private static String return_type;
  
  private static String types;
  
  public static FunctionnormalizerEntitiesTranslator getInstance(final Locale locale) {
    if(instance == null) {
    	instance = new org.eclipse.osbp.utils.functionnormalizer.entities.FunctionnormalizerEntitiesTranslator();
    	if (lastLocale == null) {
    		instance.changeLocale(locale);
    	}
    }
    return instance;
  }
  
  public void addPropertyChangeListener(final PropertyChangeListener listener) {
    this.pcs.addPropertyChangeListener(listener);
  }
  
  public void removePropertyChangeListener(final PropertyChangeListener listener) {
    this.pcs.removePropertyChangeListener(listener);
  }
  
  public void mergeTranslations() {
    
  }
  
  public HashMap<String, String> getTranslations() {
    return translations;
  }
  
  public String getFunction_normalizer() {
    if (log.isDebugEnabled()) log.debug("getFunction_normalizer returns:"+this.function_normalizer);
    return this.function_normalizer;
  }
  
  public String getFunction_type() {
    if (log.isDebugEnabled()) log.debug("getFunction_type returns:"+this.function_type);
    return this.function_type;
  }
  
  public String getFunction_typing() {
    if (log.isDebugEnabled()) log.debug("getFunction_typing returns:"+this.function_typing);
    return this.function_typing;
  }
  
  public String getName() {
    if (log.isDebugEnabled()) log.debug("getName returns:"+this.name);
    return this.name;
  }
  
  public String getNormalizer() {
    if (log.isDebugEnabled()) log.debug("getNormalizer returns:"+this.normalizer);
    return this.normalizer;
  }
  
  public String getReturn_type() {
    if (log.isDebugEnabled()) log.debug("getReturn_type returns:"+this.return_type);
    return this.return_type;
  }
  
  public String getTypes() {
    if (log.isDebugEnabled()) log.debug("getTypes returns:"+this.types);
    return this.types;
  }
  
  public void setFunction_normalizer(final String newFunction_normalizer) {
    if (log.isDebugEnabled()) log.debug("setFunction_normalizer called:"+newFunction_normalizer);
    String oldFunction_normalizer = this.function_normalizer;
    this.function_normalizer = newFunction_normalizer;
    translations.put("org.eclipse.osbp.utils.functionnormalizer.entities.function_normalizer", newFunction_normalizer);
    this.pcs.firePropertyChange("function_normalizer", oldFunction_normalizer, newFunction_normalizer);
  }
  
  public void setFunction_type(final String newFunction_type) {
    if (log.isDebugEnabled()) log.debug("setFunction_type called:"+newFunction_type);
    String oldFunction_type = this.function_type;
    this.function_type = newFunction_type;
    translations.put("org.eclipse.osbp.utils.functionnormalizer.entities.function_type", newFunction_type);
    this.pcs.firePropertyChange("function_type", oldFunction_type, newFunction_type);
  }
  
  public void setFunction_typing(final String newFunction_typing) {
    if (log.isDebugEnabled()) log.debug("setFunction_typing called:"+newFunction_typing);
    String oldFunction_typing = this.function_typing;
    this.function_typing = newFunction_typing;
    translations.put("org.eclipse.osbp.utils.functionnormalizer.entities.function_typing", newFunction_typing);
    this.pcs.firePropertyChange("function_typing", oldFunction_typing, newFunction_typing);
  }
  
  public void setName(final String newName) {
    if (log.isDebugEnabled()) log.debug("setName called:"+newName);
    String oldName = this.name;
    this.name = newName;
    translations.put("org.eclipse.osbp.utils.functionnormalizer.entities.name", newName);
    this.pcs.firePropertyChange("name", oldName, newName);
  }
  
  public void setNormalizer(final String newNormalizer) {
    if (log.isDebugEnabled()) log.debug("setNormalizer called:"+newNormalizer);
    String oldNormalizer = this.normalizer;
    this.normalizer = newNormalizer;
    translations.put("org.eclipse.osbp.utils.functionnormalizer.entities.normalizer", newNormalizer);
    this.pcs.firePropertyChange("normalizer", oldNormalizer, newNormalizer);
  }
  
  public void setReturn_type(final String newReturn_type) {
    if (log.isDebugEnabled()) log.debug("setReturn_type called:"+newReturn_type);
    String oldReturn_type = this.return_type;
    this.return_type = newReturn_type;
    translations.put("org.eclipse.osbp.utils.functionnormalizer.entities.return_type", newReturn_type);
    this.pcs.firePropertyChange("return_type", oldReturn_type, newReturn_type);
  }
  
  public void setTypes(final String newTypes) {
    if (log.isDebugEnabled()) log.debug("setTypes called:"+newTypes);
    String oldTypes = this.types;
    this.types = newTypes;
    translations.put("org.eclipse.osbp.utils.functionnormalizer.entities.types", newTypes);
    this.pcs.firePropertyChange("types", oldTypes, newTypes);
  }
  
  public void changeLocale(final Locale locale) {
    // avoid unnecessary settings
    if (locale == null) {
    	if (log.isDebugEnabled()) log.debug("locale for org.eclipse.osbp.utils.functionnormalizer.entities is null.");
    	return;
    }
    if (locale.equals(lastLocale)) {
    	if (log.isDebugEnabled()) log.debug("locale for org.eclipse.osbp.utils.functionnormalizer.entities already set to "+locale.getDisplayLanguage());
    	return;
    }
    if (log.isDebugEnabled()) log.debug("locale for org.eclipse.osbp.utils.functionnormalizer.entities set to "+locale.getDisplayLanguage());
    lastLocale = locale;
    // call the imported translators change locale method
    try {
    	java.util.ResourceBundle resource = java.util.ResourceBundle.getBundle("i18n.I18N", locale, getClass().getClassLoader());
    	try {
    		setFunction_normalizer(resource.getString("org.eclipse.osbp.utils.functionnormalizer.entities.function_normalizer"));
    	} catch (Exception e) {
    		log.error(getClass().getCanonicalName()+" - "+e.getLocalizedMessage());
    	}
    	try {
    		setFunction_type(resource.getString("org.eclipse.osbp.utils.functionnormalizer.entities.function_type"));
    	} catch (Exception e) {
    		log.error(getClass().getCanonicalName()+" - "+e.getLocalizedMessage());
    	}
    	try {
    		setFunction_typing(resource.getString("org.eclipse.osbp.utils.functionnormalizer.entities.function_typing"));
    	} catch (Exception e) {
    		log.error(getClass().getCanonicalName()+" - "+e.getLocalizedMessage());
    	}
    	try {
    		setName(resource.getString("org.eclipse.osbp.utils.functionnormalizer.entities.name"));
    	} catch (Exception e) {
    		log.error(getClass().getCanonicalName()+" - "+e.getLocalizedMessage());
    	}
    	try {
    		setNormalizer(resource.getString("org.eclipse.osbp.utils.functionnormalizer.entities.normalizer"));
    	} catch (Exception e) {
    		log.error(getClass().getCanonicalName()+" - "+e.getLocalizedMessage());
    	}
    	try {
    		setReturn_type(resource.getString("org.eclipse.osbp.utils.functionnormalizer.entities.return_type"));
    	} catch (Exception e) {
    		log.error(getClass().getCanonicalName()+" - "+e.getLocalizedMessage());
    	}
    	try {
    		setTypes(resource.getString("org.eclipse.osbp.utils.functionnormalizer.entities.types"));
    	} catch (Exception e) {
    		log.error(getClass().getCanonicalName()+" - "+e.getLocalizedMessage());
    	}
    } catch (java.util.MissingResourceException mre) {
    	System.err.println(getClass().getCanonicalName()+" - "+mre.getLocalizedMessage());
    }
    
  }
}
