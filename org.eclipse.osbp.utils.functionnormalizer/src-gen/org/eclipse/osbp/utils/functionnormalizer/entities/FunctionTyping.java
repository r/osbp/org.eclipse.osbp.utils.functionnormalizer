/**
 * @date           08.07.2014
 *  @author         dominguez
 */
package org.eclipse.osbp.utils.functionnormalizer.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import org.eclipse.osbp.dsl.common.datatypes.IBean;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.utils.functionnormalizer.entities.FunctionNormalizer;
import org.eclipse.osbp.utils.functionnormalizer.entities.FunctionType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Embeddable
@SuppressWarnings("all")
public class FunctionTyping implements Serializable, IBean {
  @Transient
  @Dispose
  private boolean disposed;
  
  @XmlElementWrapper(name = "functionType")
  @XmlElement(name = "key")
  @Basic
  @Embedded
  @ElementCollection
  @Valid
  private List<FunctionType> types;
  
  @XmlElementWrapper(name = "normalizer")
  @XmlElement(name = "function")
  @Basic
  @Embedded
  @ElementCollection
  @Valid
  private List<FunctionNormalizer> normalizer;
  
  /**
   * @return true, if the object is disposed. 
   * Disposed means, that it is prepared for garbage collection and may not be used anymore. 
   * Accessing objects that are already disposed will cause runtime exceptions.
   * 
   */
  @Dispose
  public boolean isDisposed() {
    return this.disposed;
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    disposed = true;
  }
  
  /**
   * @return Returns an unmodifiable list of types.
   */
  public List<FunctionType> getTypes() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetTypes());
  }
  
  /**
   * Sets the given types to the object. Currently contained types instances will be removed.
   * 
   * @param types the list of new instances
   */
  public void setTypes(final List<FunctionType> types) {
    // remove the old functionType
    for(FunctionType oldElement : new ArrayList<FunctionType>(this.internalGetTypes())){
      removeFromTypes(oldElement);
    }
    
    // add the new functionType
    for(FunctionType newElement : types){
      addToTypes(newElement);
    }
  }
  
  /**
   * Returns the list of <code>FunctionType</code>s thereby lazy initializing it.
   */
  private List<FunctionType> internalGetTypes() {
    if (this.types == null) {
      this.types = new ArrayList<FunctionType>();
    }
    return this.types;
  }
  
  /**
   * Adds the given functionType to this object. <p>
   * 
   */
  public void addToTypes(final FunctionType functionType) {
    checkDisposed();
    if (!getTypes().contains(functionType)){
      internalAddToTypes(functionType);
    }
    
  }
  
  public void removeFromTypes(final FunctionType functionType) {
    checkDisposed();
    internalGetTypes().remove(functionType);
  }
  
  /**
   * For internal use only!
   */
  void internalAddToTypes(final FunctionType functionType) {
    internalGetTypes().add(functionType);
  }
  
  /**
   * For internal use only!
   */
  void internalRemoveFromTypes(final FunctionType functionType) {
    internalGetTypes().remove(functionType);
  }
  
  /**
   * @return Returns an unmodifiable list of normalizer.
   */
  public List<FunctionNormalizer> getNormalizer() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetNormalizer());
  }
  
  /**
   * Sets the given normalizer to the object. Currently contained normalizer instances will be removed.
   * 
   * @param normalizer the list of new instances
   */
  public void setNormalizer(final List<FunctionNormalizer> normalizer) {
    // remove the old functionNormalizer
    for(FunctionNormalizer oldElement : new ArrayList<FunctionNormalizer>(this.internalGetNormalizer())){
      removeFromNormalizer(oldElement);
    }
    
    // add the new functionNormalizer
    for(FunctionNormalizer newElement : normalizer){
      addToNormalizer(newElement);
    }
  }
  
  /**
   * Returns the list of <code>FunctionNormalizer</code>s thereby lazy initializing it.
   */
  private List<FunctionNormalizer> internalGetNormalizer() {
    if (this.normalizer == null) {
      this.normalizer = new ArrayList<FunctionNormalizer>();
    }
    return this.normalizer;
  }
  
  /**
   * Adds the given functionNormalizer to this object. <p>
   * 
   */
  public void addToNormalizer(final FunctionNormalizer functionNormalizer) {
    checkDisposed();
    if (!getNormalizer().contains(functionNormalizer)){
      internalAddToNormalizer(functionNormalizer);
    }
    
  }
  
  public void removeFromNormalizer(final FunctionNormalizer functionNormalizer) {
    checkDisposed();
    internalGetNormalizer().remove(functionNormalizer);
  }
  
  /**
   * For internal use only!
   */
  void internalAddToNormalizer(final FunctionNormalizer functionNormalizer) {
    internalGetNormalizer().add(functionNormalizer);
  }
  
  /**
   * For internal use only!
   */
  void internalRemoveFromNormalizer(final FunctionNormalizer functionNormalizer) {
    internalGetNormalizer().remove(functionNormalizer);
  }
}
