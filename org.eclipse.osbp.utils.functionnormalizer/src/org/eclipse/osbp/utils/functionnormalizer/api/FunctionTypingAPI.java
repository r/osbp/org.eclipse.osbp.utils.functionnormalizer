/*
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */

package org.eclipse.osbp.utils.functionnormalizer.api;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

//import javax.persistence.Id;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.eclipse.osbp.utils.functionnormalizer.entities.FunctionNormalizer;
import org.eclipse.osbp.utils.functionnormalizer.entities.FunctionType;
import org.eclipse.osbp.utils.functionnormalizer.entities.FunctionTyping;

/**
 * BlobTypingAPI is a API that provides predefined blob typing data that are
 * required to be able to work with Blob objects.
 * 
 * The predefined data stored in a blob typing xml file will be read out and
 * will be filled into the corresponding blob typing java objects via JAXB.
 * 
 * @date   08.07.2014
 * @author dominguez
 * 
 */
public class FunctionTypingAPI {

	private FunctionTyping functionTyping;
	private final int FUNCTION_CSS = 0;
	private final int FUNCTION_CONVERTER = 1;
	private final int FUNCTION_IMAGE_PICKER = 2;
	
	/**
	 * Preferred constructor without any required parameter due to best knowing
	 * path to the input xml file.
	 */
	public FunctionTypingAPI() {
		try {
			URL fileURL = new URL(
					"platform:/plugin/org.eclipse.osbp.utils.functionnormalizer/org/eclipse/osbp/utils/functionnormalizer/FunctionTypes.xml");
//			File xmlFile = new File(FileLocator.resolve(fileURL).toURI());
//			blobTyping = readBlob(xmlFile);
			InputStream inputStream = fileURL.openConnection().getInputStream();
			functionTyping = readFunction(inputStream);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
//		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Constructor in case of a different path to the input xml file.
	 * 
	 * !!! This is also the only usable constructor for JUnit test because the
	 * above OSGI-path to the input xml file is not working for simple JUnit
	 * tests!!!
	 * 
	 * @param xmlFile
	 */
	public FunctionTypingAPI(File xmlFile) {
		try {
			functionTyping = readFunction(xmlFile);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Method that reads the blob typing xml file and fill the 'BlobTyping'
	 * object via JAXB with all the receiving data from the xml input file.
	 * 
	 * @param xmlFile
	 * @return the {@link BlobTyping} object filled with all the defined data
	 *         from the blob typing xml file.
	 * @throws JAXBException
	 */
	private FunctionTyping readFunction(File xmlFile) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(FunctionTyping.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		return (FunctionTyping) jaxbUnmarshaller.unmarshal(xmlFile);
	}

	/**
	 * Method that reads the blob typing xml file and fill the 'BlobTyping'
	 * object via JAXB with all the receiving data from the xml input file.
	 * 
	 * @param xmlFile
	 * @return the {@link BlobTyping} object filled with all the defined data
	 *         from the blob typing xml file.
	 * @throws JAXBException
	 */
	private FunctionTyping readFunction(InputStream xmlFile) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(FunctionTyping.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		return (FunctionTyping) jaxbUnmarshaller.unmarshal(xmlFile);
	}
	
	/**
	 * Provides the defined list of function normalizer objects.
	 * 
	 * @return list of {@link FunctionNormalizer}
	 */
	public List<FunctionNormalizer> getNormalizer() {
		return functionTyping.getNormalizer();
	}

	/**
	 * Provides the defined list of function type objects.
	 * 
	 * @return list of {@link FunctionNormalizer}
	 */
	public List<FunctionType> getTypes() {
		return functionTyping.getTypes();
	}

	/**
	 * Provides the name of the function type object 'functionCss'.
	 * 
	 * @return Name of {@link FunctionType}
	 */
	public String getFunctionCssTypeName() {
		return functionTyping.getTypes().get(FUNCTION_CSS).getName();
	}
	
	/**
	 * Provides the name of the function type object 'functionConverter'.
	 * 
	 * @return Name of {@link FunctionType}
	 */
	public String getFunctionConverterTypeName() {
		return functionTyping.getTypes().get(FUNCTION_CONVERTER).getName();
	}
	
	/**
	 * Provides the name of the function type object 'functionImagePicker'.
	 * 
	 * @return Name of {@link FunctionType}
	 */
	public String getFunctionImagePickerTypeName() {
		return functionTyping.getTypes().get(FUNCTION_IMAGE_PICKER).getName();
	}
	
	/**
	 * Provides the function normalizer name for an individual position within
	 * the list of all the defined function objects of the normalizer object.
	 * 
	 * @param index
	 * @return the function normalizer name as {@link String} or ""(blank) if
	 *         not found
	 */
	public String getFunctionNormalizerNameByListIndex(int index) {
		int length = functionTyping.getNormalizer().size();
		if (index < length) {
			return functionTyping.getNormalizer().get(index).getName();
		} else {
			return "";
		}
	}

	/**
	 * Provides the function normalizer return type for an individual position within the
	 * list of all the defined function objects of the normalizer object.
	 * 
	 * @param index
	 * @return the normalizer resolution as {@link String} or ""(blank) if not
	 *         found
	 */
	public String getFunctionNormalizerReturnTypeByListIndex(int index) {
		int length = functionTyping.getNormalizer().size();
		if (index < length) {
			return functionTyping.getNormalizer().get(index).getReturnType();
		} else {
			return "";
		}
	}

	/**
	 * Provides the function normalizer return type for a specific function name.
	 * 
	 * @param resolutionStr
	 * @return the normalizer return type as {@link String} or ""(blank) if not found
	 */
	public String getFunctionNormalizerReturnTypeByName(String functionName) {
		for (FunctionNormalizer functionNormalizer : functionTyping
				.getNormalizer()) {
			if (functionNormalizer.getName().equals(functionName)) {
				return functionNormalizer.getReturnType();
			}
		}
		return "";
	}
	
	/**
	 * Provides the function type name for an individual position within the
	 * list of all the defined function objects of the functionType object.
	 * 
	 * @param index
	 * @return the function type name as {@link String} or ""(blank) if not
	 *         found
	 */
	public String getFunctionTypeNameByListIndex(int index) {
		int length = functionTyping.getTypes().size();
		if (index < length) {
			return functionTyping.getTypes().get(index).getName();
		} else {
			return "";
		}
	}

}
